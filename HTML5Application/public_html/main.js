window.onload = function () {
//
//
////var node = {};
//
//    node["id"] = "n0";
//    node["label"] = "node1";
//    node["x"] = 1;
//    node["y"] = 2;
//    node["size"] = 2;
    let nodes = [];
    let edges = [];
    let s;
//var edge = {};
//    edge["id"] = "e0";
//    edge["source"] = "n0";
//    edge["target"] = "n1";
    document.getElementById('edgeBtn').addEventListener('click', function () {
        let id = document.getElementById('idedge').value;
        let  source = document.getElementById('from').value;
        let target = document.getElementById('to').value;
        let newEdge = new Edge(id, source, target);
        edges.push(newEdge);
        document.getElementById('idedge').value = '';
        document.getElementById('from').value = '';
        document.getElementById('to').value = '';
        console.log(edges);
        
        drawGraph();

    });
    document.getElementById('apexBtn').addEventListener('click', function () {
        let id = document.getElementById('id').value;
        let label = document.getElementById('label').value;
        let size = document.getElementById('size').value;
        let x = document.getElementById('positionX').value;
        let y = document.getElementById('positionY').value;
        let newNode = new Node(id, label, x, y,size);
        let oryginal =true;
        for( let i = 0; i < nodes.length; i++ ){
            if( nodes[ i ][ 'id' ] == id ){
                document.getElementById('id').value = '';
                document.getElementById('label').value = '';
                document.getElementById('size').value = '';
                document.getElementById('positionX').value = '';
                document.getElementById('positionY').value = '';
                alert("wierzchołek o tym id już istnieje");
                oryginal =  false;
                break;
            } 
        };
        if( oryginal ){
        nodes.push( newNode );
    }
        document.getElementById('id').value = '';
        document.getElementById('label').value = '';
        document.getElementById('size').value = '';
        document.getElementById('positionX').value = '';
        document.getElementById('positionY').value = '';
        console.log(nodes);
        
        drawGraph();

    });
    let Node = function ( id, label,x ,y ,size ){
        this['id'] = id;
        this['label'] = label;
        this['x'] = x;
        this['y'] = y;
        this['size'] = size;
    };
    let Edge = function (id, source, target) {
        this['id'] = id;
        this['source'] = source;
        this['target'] = target;
    };
    function drawGraph() {
        document.getElementById('graf').innerHTML = "";
        var data = {};
        data['edges'] = edges;
        data['nodes'] = nodes;

        s = new sigma({
            graph: data,
            renderer: {
                container: document.getElementById('graf'),
                type: 'canvas'
            },
            settings: {
                doubleClickEnabled: false,
                minEdgeSize: 0.5,
                maxEdgeSize: 4,
                enableEdgeHovering: true,
                edgeHoverColor: 'edge',
                defaultEdgeHoverColor: '#000',
                edgeHoverSizeRatio: 1,
                edgeHoverExtremities: true
            }
        });
        
    }
    
};
// cosnole.log(edges);
//let Node = ()=>{
//    
//}
